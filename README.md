# operator

An operator to deploy FlatTrack instances

## Description

The operator provisions a Knative Service and a small Postgres database.

## Getting Started

### Prerequisites

- go version v1.25.5+
- ko version v0.16.0+
- kubectl version v1.30.0+.
- Kubernetes v1.30.0+ cluster
  - Knative Serving version v0.15.0+
  - cert-manager version v1.15.1
  - cloudnative-pg v1.24.0

### To Deploy on the cluster
**Build and push your image to the location specified by `IMG`:**

```sh
make image IMG=<some-registry>/operator:tag
```

**NOTE:** This image ought to be published in the personal registry you specified. 
And it is required to have access to pull the image from the working environment. 
Make sure you have the proper permission to the registry if the above commands don’t work.

**Install the CRDs into the cluster:**

```sh
make install
```

**Deploy the Manager to the cluster with the image specified by `IMG`:**

```sh
make deploy IMG=<some-registry>/operator:tag
```

> **NOTE**: If you encounter RBAC errors, you may need to grant yourself cluster-admin 
privileges or be logged in as admin.

**Create instances of your solution**
You can apply the samples (examples) from the config/sample:

```sh
kubectl apply -k config/samples/
```

>**NOTE**: Ensure that the samples has default values to test it out.

**Important: set up a backup-bucket Secret:**

A Secret named _backup-bucket_ in the same namespace as a FlatTrack instance is used for automatic backups, to encrypt then push the backup to a target S3 style object storage bucket.

Create a Secret, in the namespaces where FlatTrack instances are, like so

```yaml
---
apiVersion: v1
stringData:
  ACCESS_KEY: your access key here
  SECRET_KEY: your secret key here
  AGE_PUBLIC_KEY: age, rsa or ed25519 public key here
  DESTINATION_PATH: bucket path
  ENDPOINT: s3 style endpoint
kind: Secret
metadata:
  name: backup-bucket
  namespace: default
type: Opaque
```

### To Uninstall
**Delete the instances (CRs) from the cluster:**

```sh
kubectl delete -k config/samples/
```

**Delete the APIs(CRDs) from the cluster:**

```sh
make uninstall
```

**UnDeploy the controller from the cluster:**

```sh
make undeploy
```

## Contributing
// TODO(user): Add detailed information on how you would like others to contribute to this project

**NOTE:** Run `make --help` for more information on all potential `make` targets

More information can be found via the [Kubebuilder Documentation](https://book.kubebuilder.io/introduction.html)

## Local development environment

see [hack/local-dev/README.org](./hack/local-dev/README.org).

## License

Copyright 2024 Flattrack authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

