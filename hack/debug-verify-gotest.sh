#!/bin/sh

set -o errexit
set -o nounset
set -x

kubectl get ns,pods,services,cronjobs,secrets,configmaps,services.serving.knative.dev -A
kubectl get flattracks -A -o yaml

# NOTE does Knative work?
kubectl apply -f - <<EOF
apiVersion: serving.knative.dev/v1
kind: Service
metadata:
  name: nginx
  namespace: default
spec:
  template:
    spec:
      containers:
      - image: cgr.dev/chainguard/nginx:latest
EOF

RETRIES=0
until [ "$RETRIES" = 10 ]; do
    URL="$(kubectl get services.serving.knative.dev nginx -o=jsonpath='{.status.url}')"
    curl -sSLv "$URL" | grep -z 'Welcome to nginx'
    if [ "$?" = 0 ]; then
        break
    fi
    RETRIES=$((RETRIES+=1))
done
