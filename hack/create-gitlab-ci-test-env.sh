#!/bin/sh

set -o errexit
set -o nounset
set -x

cd "$(git rev-parse --show-toplevel)" || exit 1

# NOTE this is a horrible hack
#      for some reason `dig` doesn't resolve CI services in GitLab CI
DOCKER_SERVICE_IP="$(curl -v http://docker:2375 2>&1 | grep 'IPv4:' | cut -d' ' -f3 || true)"

kind create cluster --config ./hack/local-dev/kind-config-gitlab-ci.yaml
sed -i -E -e 's/localhost|0\.0\.0\.0|127\.0\.0\.1/docker/g' "$HOME/.kube/config"
kubectl --context kind-kind cluster-info

cd ./hack/local-dev/
[ -f /tmp/e-operator-flattrack-io.age ] || age-keygen -o /tmp/e-operator-flattrack-io.age
mkdir -p cm/
grep public /tmp/e-operator-flattrack-io.age | cut -d: -f2 | sed 's/^ //g' > cm/AGE_PUBLIC_KEY
kustomize edit remove configmap config-domain --namespace knative-serving
kustomize edit add configmap config-domain --behavior=merge --namespace knative-serving --from-literal="$DOCKER_SERVICE_IP.sslip.io"=""
until kustomize build . | kubectl apply --server-side --force-conflicts -f -; do sleep 1s; done

kubectl --context kind-kind wait --for=condition=Established --all crd
until kubectl --context kind-kind -n knative-serving get deployment controller; do
    sleep 1s;
done
until [ "$(kubectl --context kind-kind -n knative-serving get deployment controller -o=jsonpath='{.status.readyReplicas}')" = 1 ]; do
    sleep 1s;
done
kubectl --context kind-kind -n knative-serving wait pod --for=condition=Ready -l '!job-name' --timeout=10m

