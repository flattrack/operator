#!/usr/bin/env bash

set -o errexit
set -o nounset

cd "$(dirname "$(realpath "$0")" )" || exit 1

kind get clusters | grep -q kind 2>&1 >/dev/null || kind create cluster --config ./kind-config.yaml

[ -f /tmp/e-operator-flattrack-io.age ] || age-keygen -o /tmp/e-operator-flattrack-io.age
mkdir -p cm/
grep public /tmp/e-operator-flattrack-io.age | cut -d: -f2 | sed 's/^ //g' > cm/AGE_PUBLIC_KEY
until kustomize build . | kubectl --context kind-kind apply --server-side --force-conflicts -f -; do sleep 1; done
