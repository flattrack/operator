#!/bin/sh -x

set -o errexit
set -o nounset
set -o pipefail

cd "$(git rev-parse --show-toplevel)" || exit 0

IMAGE="$(yq e '.variables["IMAGE_GOLANG_ALPINE"]' .gitlab-ci.yml)"

C_DIR="/builds/$(basename $PWD)"
podman run --rm --network=host \
    -v "$PWD:$C_DIR:ro" --workdir "$C_DIR" \
    "$IMAGE" \
      sh -c "
echo 'https://dl-cdn.alpinelinux.org/alpine/edge/testing' | tee -a /etc/apk/repositories ;
apk add --no-cache curl cosign ko git jq;
git config --global --add safe.directory $C_DIR ;
export KO_DOCKER_REPO=${KO_DOCKER_REPO:-localhost:5001/flattrack-operator}
./hack/publish.sh ${*:-}
"
