package common

import (
	"reflect"
	"testing"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/labels"
)

func TestMergeLabels(t *testing.T) {
	type args struct {
		ls []labels.Set
	}
	tests := []struct {
		name string
		args args
		want labels.Set
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MergeLabels(tt.args.ls...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MergeLabels() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMergeResourceRequirements(t *testing.T) {
	type args struct {
		rrs []*v1.ResourceRequirements
	}
	tests := []struct {
		name string
		args args
		want v1.ResourceRequirements
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MergeResourceRequirements(tt.args.rrs...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MergeResourceRequirements() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetExtraInstanceLabels(t *testing.T) {
	tests := []struct {
		name string
		want labels.Set
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetExtraInstanceLabels(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetExtraInstanceLabels() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRandString(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RandString(tt.args.n); got != tt.want {
				t.Errorf("RandString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetEnvOrDefault(t *testing.T) {
	type args struct {
		key          string
		defaultValue string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetEnvOrDefault(tt.args.key, tt.args.defaultValue); got != tt.want {
				t.Errorf("GetEnvOrDefault() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHashOfSecretData(t *testing.T) {
	type args struct {
		i map[string][]byte
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HashOfSecretData(tt.args.i); got != tt.want {
				t.Errorf("HashOfSecretData() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultImage(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultImage(); got != tt.want {
				t.Errorf("GetDefaultImage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultImagePostgres(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultImagePostgres(); got != tt.want {
				t.Errorf("GetDefaultImagePostgres() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultImageCurl(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultImageCurl(); got != tt.want {
				t.Errorf("GetDefaultImageCurl() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultImageMinio(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultImageMinio(); got != tt.want {
				t.Errorf("GetDefaultImageMinio() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultPersistentVolumeSize(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultPersistentVolumeSize(); got != tt.want {
				t.Errorf("GetDefaultPersistentVolumeSize() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultAppLoginMessage(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultAppLoginMessage(); got != tt.want {
				t.Errorf("GetDefaultAppLoginMessage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultAppSetupMessage(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultAppSetupMessage(); got != tt.want {
				t.Errorf("GetDefaultAppSetupMessage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultFlatTrackResourceRequirements(t *testing.T) {
	tests := []struct {
		name string
		want v1.ResourceRequirements
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultFlatTrackResourceRequirements(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetDefaultFlatTrackResourceRequirements() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultPostgresResourceRequirements(t *testing.T) {
	tests := []struct {
		name string
		want v1.ResourceRequirements
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultPostgresResourceRequirements(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetDefaultPostgresResourceRequirements() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultBackupCronJobResourceRequirements(t *testing.T) {
	tests := []struct {
		name string
		want v1.ResourceRequirements
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultBackupCronJobResourceRequirements(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetDefaultBackupCronJobResourceRequirements() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultSchedulerCronJobResourceRequirements(t *testing.T) {
	tests := []struct {
		name string
		want v1.ResourceRequirements
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultSchedulerCronJobResourceRequirements(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetDefaultSchedulerCronJobResourceRequirements() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultSchedulerCronJobSchedule(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultSchedulerCronJobSchedule(); got != tt.want {
				t.Errorf("GetDefaultSchedulerCronJobSchedule() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetDefaultBackupCronJobSchedule(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetDefaultBackupCronJobSchedule(); got != tt.want {
				t.Errorf("GetDefaultBackupCronJobSchedule() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetBackupBucketSecretName(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetBackupBucketSecretName(); got != tt.want {
				t.Errorf("GetBackupBucketSecretName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetInstanceInitialized(t *testing.T) {
	type args struct {
		baseDomain string
	}
	tests := []struct {
		name    string
		args    args
		want    *bool
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetInstanceInitialized(tt.args.baseDomain)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetInstanceInitialized() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetInstanceInitialized() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostRegisterInstance(t *testing.T) {
	type args struct {
		registrationURL string
		registration    Registration
	}
	tests := []struct {
		name    string
		args    args
		want    *string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := PostRegisterInstance(tt.args.registrationURL, tt.args.registration)
			if (err != nil) != tt.wantErr {
				t.Errorf("PostRegisterInstance() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("PostRegisterInstance() = %v, want %v", got, tt.want)
			}
		})
	}
}
