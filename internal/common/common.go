package common

import (
	"bytes"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"time"

	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/apimachinery/pkg/labels"
)

var (
	defaultImage                         = "registry.gitlab.com/flattrack/flattrack:0.18.4@sha256:ec398500261a2db0c159436a4a1e93105d9ba110ce751b57762366e2317a7910"
	defaultImagePostgres                 = "ghcr.io/cloudnative-pg/postgresql:16.4@sha256:9447bc8a82997990a70cfae710ee3a5fab4bf62f5122c06cc7b74945a2dc09df"
	defaultImageCurl                     = "cgr.dev/chainguard/curl@sha256:33e0bd8828880ec090879af7647b3dcdba30453f08bccdcd74f6768e2da82ef0"
	defaultImageMinio                    = "docker.io/minio/mc:RELEASE.2024-02-09T22-18-24Z@sha256:57cf0b17ee9cbb2c841898d3aab5722ba5e30c7c097f30e10833b286bce43110"
	defaultImageAge                      = "registry.gitlab.com/bobymcbobs/container-images/age@sha256:7c5039a05d2d573b2c894647397cc4c516929d023e130eb7cfc0983b1b55dd19"
	defaultPersistentVolumeSize          = "1Gi"
	defaultSchedulerCronJobSchedule      = "0 */1 * * *"
	defaultTimezone                      = "Etc/UTC"
	defaultBackupCronJobSchedule         = "0 0 * * */4"
	defaultBackupBucketSecretName        = "backup-bucket"
	defaultFlatTrackResourceRequirements = v1.ResourceRequirements{
		Requests: v1.ResourceList{
			v1.ResourceCPU:    resource.MustParse("100m"),
			v1.ResourceMemory: resource.MustParse("50Mi"),
		},
		Limits: v1.ResourceList{
			v1.ResourceCPU:    resource.MustParse("100m"),
			v1.ResourceMemory: resource.MustParse("50Mi"),
		},
	}
	defaultPostgresResourceRequirements = v1.ResourceRequirements{
		Requests: v1.ResourceList{
			v1.ResourceCPU:    resource.MustParse("200m"),
			v1.ResourceMemory: resource.MustParse("100Mi"),
		},
		Limits: v1.ResourceList{
			v1.ResourceCPU:    resource.MustParse("200m"),
			v1.ResourceMemory: resource.MustParse("100Mi"),
		},
	}
	defaultBackupCronJobResourceRequirements = v1.ResourceRequirements{
		Requests: v1.ResourceList{
			v1.ResourceCPU:    resource.MustParse("100m"),
			v1.ResourceMemory: resource.MustParse("50Mi"),
		},
		Limits: v1.ResourceList{
			v1.ResourceCPU:    resource.MustParse("100m"),
			v1.ResourceMemory: resource.MustParse("50Mi"),
		},
	}
	defaultSchedulerCronJobResourceRequirements = v1.ResourceRequirements{
		Requests: v1.ResourceList{
			v1.ResourceCPU:    resource.MustParse("100m"),
			v1.ResourceMemory: resource.MustParse("50Mi"),
		},
		Limits: v1.ResourceList{
			v1.ResourceCPU:    resource.MustParse("100m"),
			v1.ResourceMemory: resource.MustParse("50Mi"),
		},
	}
)

func MergeLabels(ls ...labels.Set) labels.Set {
	mergedMap := labels.Set{}
	for _, s := range ls {
		for k, v := range s {
			mergedMap[k] = v
		}
	}
	return mergedMap
}

func MergeResourceRequirements(rrs ...*v1.ResourceRequirements) v1.ResourceRequirements {
	mergedResourceRequirements := v1.ResourceRequirements{}
	for _, r := range rrs {
		if r == nil {
			continue
		}
		for k, v := range r.Requests {
			if mergedResourceRequirements.Requests == nil {
				mergedResourceRequirements.Requests = v1.ResourceList{}
			}
			mergedResourceRequirements.Requests[k] = v
		}
		for k, v := range r.Limits {
			if mergedResourceRequirements.Limits == nil {
				mergedResourceRequirements.Limits = v1.ResourceList{}
			}
			mergedResourceRequirements.Limits[k] = v
		}
	}
	return mergedResourceRequirements
}

func GetExtraInstanceLabels() labels.Set {
	labelsAsString := GetEnvOrDefault("FLATTRACK_OPERATOR_EXTRA_INSTANCE_LABELS", "")
	if labelsAsString == "" {
		return labels.Set{}
	}
	ls, err := labels.ConvertSelectorToLabelsMap(labelsAsString)
	if err != nil {
		return labels.Set{}
	}
	return ls
}

func RandString(n int) string {
	const letterBytes = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ"
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func GetEnvOrDefault(key string, defaultValue string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultValue
}

func HashOfSecretData(i map[string][]byte) string {
	v, _ := json.Marshal(i)
	return fmt.Sprintf("%x", md5.Sum(v))
}

func GetDefaultImage() string {
	return GetEnvOrDefault("FLATTRACK_OPERATOR_DEFAULT_IMAGE", defaultImage)
}

func GetDefaultImagePostgres() string {
	return GetEnvOrDefault("FLATTRACK_OPERATOR_DEFAULT_IMAGE_POSTGRES", defaultImagePostgres)
}

func GetDefaultImageCurl() string {
	return GetEnvOrDefault("FLATTRACK_OPERATOR_DEFAULT_IMAGE_CURL", defaultImageCurl)
}

func GetDefaultImageMinio() string {
	return GetEnvOrDefault("FLATTRACK_OPERATOR_DEFAULT_IMAGE_MINIO", defaultImageMinio)
}

func GetDefaultImageAge() string {
	return GetEnvOrDefault("FLATTRACK_OPERATOR_DEFAULT_IMAGE_AGE", defaultImageAge)
}

func GetDefaultPersistentVolumeSize() string {
	return GetEnvOrDefault("FLATTRACK_OPERATOR_DEFAULT_PERSISTENT_VOLUME_SIZE", defaultPersistentVolumeSize)
}

func GetDefaultAppLoginMessage() string {
	return GetEnvOrDefault("FLATTRACK_OPERATOR_DEFAULT_APP_LOGIN_MESSAGE", "")
}

func GetDefaultAppSetupMessage() string {
	return GetEnvOrDefault("FLATTRACK_OPERATOR_DEFAULT_APP_SETUP_MESSAGE", "")
}

func GetDefaultFlatTrackResourceRequirements() v1.ResourceRequirements {
	return defaultFlatTrackResourceRequirements
}

func GetDefaultPostgresResourceRequirements() v1.ResourceRequirements {
	return defaultPostgresResourceRequirements
}

func GetDefaultBackupCronJobResourceRequirements() v1.ResourceRequirements {
	return defaultBackupCronJobResourceRequirements
}

func GetDefaultSchedulerCronJobResourceRequirements() v1.ResourceRequirements {
	return defaultSchedulerCronJobResourceRequirements
}

func GetDefaultSchedulerCronJobSchedule() string {
	return defaultSchedulerCronJobSchedule
}

func GetDefaultTimezone() string {
	return GetEnvOrDefault("FLATTRACK_OPERATOR_TIMEZONE", defaultTimezone)
}

func GetDefaultBackupCronJobSchedule() string {
	return defaultBackupCronJobSchedule
}

func GetBackupBucketSecretName() string {
	return GetEnvOrDefault("FLATTRACK_OPERATOR_BACKUP_BUCKET_SECRET_NAME", defaultBackupBucketSecretName)
}

func GetPodRuntimeClassName() string {
	return GetEnvOrDefault("FLATTRACK_OPERATOR_DEFAULT_POD_RUNTIME_CLASS", "")
}

func GetInstanceInitialized(baseDomain string) (*bool, error) {
	if baseDomain == "" {
		return nil, fmt.Errorf("error: url empty for baseDomain")
	}
	b, err := url.Parse(baseDomain)
	if err != nil {
		return nil, err
	}
	b.Path = "/api/system/initialized"
	client := &http.Client{
		Timeout: 10 * time.Second,
	}
	req, err := http.NewRequest(http.MethodGet, b.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	jsonResponse := struct {
		Data *bool
	}{}
	if err := json.NewDecoder(resp.Body).Decode(&jsonResponse); err != nil {
		return nil, err
	}
	return jsonResponse.Data, nil
}

type UserSpec struct {
	Names    string `json:"names"`
	Email    string `json:"email"`
	Password string `json:"password"`
}
type Registration struct {
	User     UserSpec `json:"user"`
	Timezone string   `json:"timezone"`
	Language string   `json:"language"`
	FlatName string   `json:"flatName"`
	Secret   string   `json:"-"`
}

func PostRegisterInstance(registrationURL string, registration Registration) (*string, error) {
	b, err := url.Parse(registrationURL)
	if err != nil {
		return nil, err
	}
	b.Path = "/api/admin/register"
	client := &http.Client{}
	bodyBytes, err := json.Marshal(registration)
	if err != nil {
		return nil, err
	}
	buf := bytes.NewBuffer(bodyBytes)
	req, err := http.NewRequest(http.MethodPost, b.String(), buf)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	jsonResponse := struct {
		Spec *bool
		Data *string
	}{}
	if err := json.NewDecoder(resp.Body).Decode(&jsonResponse); err != nil {
		return nil, err
	}
	if jsonResponse.Spec == nil || (jsonResponse.Spec != nil && !*jsonResponse.Spec) {
		return nil, fmt.Errorf("failed to register")
	}
	return jsonResponse.Data, nil
}
