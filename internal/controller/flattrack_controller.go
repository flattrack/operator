/*
Copyright 2024 Flattrack authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"net/url"
	"reflect"
	"sync"
	"time"

	cnpgv1 "github.com/cloudnative-pg/cloudnative-pg/api/v1"
	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	"k8s.io/utils/ptr"
	knservingv1 "knative.dev/serving/pkg/apis/serving/v1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"

	"gitlab.com/flattrack/operator/api/v1alpha1"
	instancesv1alpha1 "gitlab.com/flattrack/operator/api/v1alpha1"
	common "gitlab.com/flattrack/operator/internal/common"
)

const finalizerName = "instances.flattrack.io/finalizer"

var (
	extraInstanceLabels = labels.Set{}
)

// FlatTrackReconciler reconciles a FlatTrack object
type FlatTrackReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	Recorder record.EventRecorder
}

func (r *FlatTrackReconciler) getServiceName(req ctrl.Request) string {
	return req.Name
}

func (r *FlatTrackReconciler) getServiceBackupName(req ctrl.Request) string {
	return req.Name + "-backup"
}

func (r *FlatTrackReconciler) getServiceSchedulerName(req ctrl.Request) string {
	return req.Name + "-scheduler"
}

func (r *FlatTrackReconciler) getPostgresName(req ctrl.Request) string {
	return req.Name + "-postgres"
}

func (r *FlatTrackReconciler) getPostgresPoolerName(req ctrl.Request) string {
	return req.Name + "-postgres-pooler-rw"
}

func (r *FlatTrackReconciler) getPostgresHostname(req ctrl.Request) string {
	return r.getPostgresName(req) + "." + req.Namespace
}

func (r *FlatTrackReconciler) makeCommonLabels(req ctrl.Request, component instancesv1alpha1.ComponentType) labels.Set {
	return map[string]string{
		"operator.flattrack.io/managed":   "true",
		"operator.flattrack.io/instance":  req.Name,
		"operator.flattrack.io/component": string(component),
	}
}

func makeRegistrationURL(ctx context.Context, instance *v1alpha1.FlatTrack, id string) (*string, error) {
	log := log.FromContext(ctx)
	if instance.Status.URL == nil {
		return nil, fmt.Errorf("error: url not yet populated")
	}
	registrationURL, err := url.Parse(*instance.Status.URL)
	if err != nil {
		log.Error(err, "failed to parse url: "+*instance.Status.URL)
		return nil, err
	}
	q := registrationURL.Query()
	q.Add("secret", id)
	registrationURL.RawQuery = q.Encode()
	registrationURL.Path = "/setup"
	return ptr.To(registrationURL.String()), nil
}

func (r *FlatTrackReconciler) reconcileSecret(ctx context.Context, req ctrl.Request, instance *instancesv1alpha1.FlatTrack) error {
	secret := &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      r.getServiceName(req),
			Namespace: req.Namespace,
		},
	}
	op, err := ctrl.CreateOrUpdate(ctx, r.Client, secret, func() error {
		secret.ObjectMeta.Labels = common.MergeLabels(
			r.makeCommonLabels(req, instancesv1alpha1.CompontentTypeFlatTrack),
			instance.ObjectMeta.Labels,
			extraInstanceLabels,
		)
		if secret.StringData == nil {
			secret.StringData = map[string]string{}
		}
		if _, hasSchedulerSecret := secret.Data["APP_SCHEDULER_ENDPOINT_SECRET"]; !hasSchedulerSecret {
			secret.StringData["APP_SCHEDULER_ENDPOINT_SECRET"] = common.RandString(45)
		}
		if _, hasRegistrationSecret := secret.Data["APP_REGISTRATION_SECRET"]; !hasRegistrationSecret {
			secret.StringData["APP_REGISTRATION_SECRET"] = common.RandString(20)
		}
		secret.ObjectMeta.Labels = common.MergeLabels(secret.ObjectMeta.Labels, instance.ObjectMeta.Labels, extraInstanceLabels)
		if err := ctrl.SetControllerReference(instance, secret, r.Scheme); err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		r.Recorder.Event(instance, v1.EventTypeWarning, string(op),
			fmt.Sprintf("%v %v/%v: %v",
				op,
				secret.Namespace,
				secret.Name, err))
		return err
	}
	if op == controllerutil.OperationResultCreated || op == controllerutil.OperationResultUpdated {
		r.Recorder.Event(instance, v1.EventTypeNormal, string(op),
			fmt.Sprintf("%v %v/%v",
				op,
				secret.Namespace,
				secret.Name))
	}
	return nil
}

func (r *FlatTrackReconciler) reconcilePostgres(ctx context.Context, req ctrl.Request, instance *instancesv1alpha1.FlatTrack) (ctrl.Result, error) {
	postgresName := r.getPostgresName(req)
	serviceName := r.getServiceName(req)

	var secret v1.Secret
	if err := r.Get(ctx, client.ObjectKey{Namespace: req.Namespace, Name: serviceName}, &secret); err != nil {
		return ctrl.Result{}, err
	}

	pgCluster := &cnpgv1.Cluster{
		ObjectMeta: metav1.ObjectMeta{
			Name:      postgresName,
			Namespace: req.Namespace,
		},
		Spec: cnpgv1.ClusterSpec{},
	}
	op, err := ctrl.CreateOrUpdate(ctx, r.Client, pgCluster, func() error {
		pgCluster.Spec.Instances = 1
		if instance.Spec.Postgres.Instances != nil {
			pgCluster.Spec.Instances = *instance.Spec.Postgres.Instances
		}
		pgCluster.Spec.ReplicationSlots = &cnpgv1.ReplicationSlotsConfiguration{}
		pgCluster.Spec.StorageConfiguration.Size = "1Gi"
		if instance.Spec.Postgres.PersistentVolumeSize != nil {
			pgCluster.Spec.StorageConfiguration.Size = *instance.Spec.Postgres.PersistentVolumeSize
		}
		pgCluster.Spec.ImageName = common.GetDefaultImagePostgres()
		if img := instance.Spec.Postgres.Image; img != nil {
			pgCluster.Spec.ImageName = *img
		}
		if rr := instance.Spec.Postgres.ResourcesRequirements; rr != nil {
			pgCluster.Spec.Resources = *rr
		}
		pgCluster.ObjectMeta.Labels = common.MergeLabels(
			r.makeCommonLabels(req, instancesv1alpha1.CompontentTypePostgres),
			instance.ObjectMeta.Labels,
			extraInstanceLabels,
		)
		if err := ctrl.SetControllerReference(instance, pgCluster, r.Scheme); err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		r.Recorder.Event(instance, v1.EventTypeWarning, string(op),
			fmt.Sprintf("%v %v/%v: %s",
				op,
				pgCluster.Namespace,
				pgCluster.Name, err))
		return ctrl.Result{}, err
	}
	if op == controllerutil.OperationResultCreated || op == controllerutil.OperationResultUpdated {
		r.Recorder.Event(instance, v1.EventTypeNormal, string(op),
			fmt.Sprintf("%v %v/%v",
				op, // TODO fix: Not Capitalised
				pgCluster.Namespace,
				pgCluster.Name))
	}
	if pgCluster.Status.ReadyInstances != pgCluster.Spec.Instances {
		return ctrl.Result{Requeue: true, RequeueAfter: 5 * time.Second}, nil
	}
	return ctrl.Result{}, nil
}

func (r *FlatTrackReconciler) reconcilePostgresPooler(ctx context.Context, req ctrl.Request, instance *instancesv1alpha1.FlatTrack) (ctrl.Result, error) {
	postgresName := r.getPostgresName(req)
	postgresPoolerName := r.getPostgresPoolerName(req)
	serviceName := r.getServiceName(req)

	var secret v1.Secret
	if err := r.Get(ctx, client.ObjectKey{Namespace: req.Namespace, Name: serviceName}, &secret); err != nil {
		return ctrl.Result{}, err
	}

	pgPooler := &cnpgv1.Pooler{
		ObjectMeta: metav1.ObjectMeta{
			Name:      postgresPoolerName,
			Namespace: req.Namespace,
		},
		Spec: cnpgv1.PoolerSpec{
			Cluster: cnpgv1.LocalObjectReference{
				Name: postgresName,
			},
		},
	}
	op, err := ctrl.CreateOrUpdate(ctx, r.Client, pgPooler, func() error {
		pgPooler.Spec.Instances = ptr.To(int32(1))
		if instance.Spec.Postgres.Pooler.Instances != nil {
			pgPooler.Spec.Instances = ptr.To(int32(*instance.Spec.Postgres.Pooler.Instances))
		}
		pgPooler.Spec.Type = "rw"
		pgPooler.Spec.PgBouncer = &cnpgv1.PgBouncerSpec{
			PoolMode: cnpgv1.PgBouncerPoolModeSession,
		}
		if rr := instance.Spec.Postgres.Pooler.ResourcesRequirements; rr != nil {
			if pgPooler.Spec.Template == nil {
				pgPooler.Spec.Template = &cnpgv1.PodTemplateSpec{}
			}
			pgPooler.Spec.Template.Spec.Containers = []v1.Container{
				{
					Name:      "pgbouncer",
					Resources: *rr,
				},
			}
		}
		pgPooler.ObjectMeta.Labels = common.MergeLabels(
			r.makeCommonLabels(req, instancesv1alpha1.CompontentTypePostgres),
			instance.ObjectMeta.Labels,
			extraInstanceLabels,
		)
		if err := ctrl.SetControllerReference(instance, pgPooler, r.Scheme); err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		r.Recorder.Event(instance, v1.EventTypeWarning, string(op),
			fmt.Sprintf("%v %v/%v: %s",
				op,
				pgPooler.Namespace,
				pgPooler.Name, err))
		return ctrl.Result{}, err
	}
	if op == controllerutil.OperationResultCreated || op == controllerutil.OperationResultUpdated {
		r.Recorder.Event(instance, v1.EventTypeNormal, string(op),
			fmt.Sprintf("%v %v/%v",
				op, // TODO fix: Not Capitalised
				pgPooler.Namespace,
				pgPooler.Name))
	}
	if pgPooler.Spec.Instances == nil || pgPooler.Status.Instances != *pgPooler.Spec.Instances {
		return ctrl.Result{Requeue: true, RequeueAfter: 5 * time.Second}, nil
	}
	return ctrl.Result{}, nil
}

func (r *FlatTrackReconciler) reconcileKnService(ctx context.Context, req ctrl.Request, instance *instancesv1alpha1.FlatTrack) error {
	serviceName := r.getServiceName(req)
	postgresName := r.getPostgresName(req)
	knsvc := &knservingv1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      serviceName,
			Namespace: req.Namespace,
		},
	}
	op, err := ctrl.CreateOrUpdate(ctx, r.Client, knsvc, func() error {
		knsvc.ObjectMeta.Labels = common.MergeLabels(
			r.makeCommonLabels(req, instancesv1alpha1.CompontentTypeFlatTrack),
			instance.ObjectMeta.Labels,
			extraInstanceLabels,
		)
		requirements := common.GetDefaultFlatTrackResourceRequirements()
		knsvc.Spec = knservingv1.ServiceSpec{
			ConfigurationSpec: knservingv1.ConfigurationSpec{
				Template: knservingv1.RevisionTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: r.makeCommonLabels(req, instancesv1alpha1.CompontentTypeFlatTrack),
						// Annotations: map[string]string{
						// 	"autoscaling.knative.dev/min-scale": "1",
						// },
					},
					Spec: knservingv1.RevisionSpec{
						PodSpec: v1.PodSpec{
							Containers: []v1.Container{{
								Name:      req.Name,
								Image:     common.GetDefaultImage(),
								Resources: common.MergeResourceRequirements(&requirements, instance.Spec.ResourcesRequirements),
								Env: []v1.EnvVar{
									{
										Name: "APP_DB_CONNECTION_STRING",
										ValueFrom: &v1.EnvVarSource{
											SecretKeyRef: &v1.SecretKeySelector{
												LocalObjectReference: v1.LocalObjectReference{
													Name: postgresName + "-app",
												},
												Key: "uri",
											},
										},
									},
									{
										Name:  "APP_LOGIN_MESSAGE",
										Value: common.GetDefaultAppLoginMessage(),
									},
									{
										Name:  "APP_SETUP_MESSAGE",
										Value: common.GetDefaultAppSetupMessage(),
									},
									{
										Name: "APP_REGISTRATION_SECRET",
										ValueFrom: &v1.EnvVarSource{
											SecretKeyRef: &v1.SecretKeySelector{
												LocalObjectReference: v1.LocalObjectReference{
													Name: serviceName,
												},
												Key: "APP_REGISTRATION_SECRET",
											},
										},
									},
									{
										Name:  "APP_SCHEDULER_DISABLE_USE_ENDPOINT",
										Value: "true",
									},
									{
										Name: "APP_SCHEDULER_ENDPOINT_SECRET",
										ValueFrom: &v1.EnvVarSource{
											SecretKeyRef: &v1.SecretKeySelector{
												LocalObjectReference: v1.LocalObjectReference{
													Name: serviceName,
												},
												Key: "APP_SCHEDULER_ENDPOINT_SECRET",
											},
										},
									},
								},
							}},
						},
					},
				},
			},
		}
		if img := instance.Spec.Image; img != nil {
			knsvc.Spec.ConfigurationSpec.Template.Spec.PodSpec.Containers[0].Image = *img
		}
		if instance.Spec.ServiceAnnotations != nil {
			knsvc.ObjectMeta.Annotations = common.MergeLabels(knsvc.ObjectMeta.Annotations, instance.Spec.ServiceAnnotations)
		}
		knsvc.ObjectMeta.Labels = common.MergeLabels(knsvc.ObjectMeta.Labels, instance.ObjectMeta.Labels, extraInstanceLabels)
		if err := ctrl.SetControllerReference(instance, knsvc, r.Scheme); err != nil {
			return err
		}
		if val := common.GetPodRuntimeClassName(); val != "" {
			knsvc.Spec.Template.Spec.RuntimeClassName = ptr.To(val)
		}
		return nil
	})
	if err != nil {
		r.Recorder.Event(instance, v1.EventTypeWarning, string(op),
			fmt.Sprintf("%v %v/%v: %v",
				op,
				knsvc.Namespace,
				knsvc.Name, err))
		return err
	}
	if op == controllerutil.OperationResultCreated || op == controllerutil.OperationResultUpdated {
		r.Recorder.Event(instance, v1.EventTypeNormal, string(op),
			fmt.Sprintf("%v %v/%v",
				op,
				knsvc.Namespace,
				knsvc.Name))
	}
	return nil
}

func (r *FlatTrackReconciler) reconcileCronjobScheduler(ctx context.Context, req ctrl.Request, instance *instancesv1alpha1.FlatTrack) error {
	serviceSchedulerName := r.getServiceSchedulerName(req)
	serviceName := r.getServiceName(req)
	cronjobScheduler := batchv1.CronJob{
		ObjectMeta: metav1.ObjectMeta{
			Name:      serviceSchedulerName,
			Namespace: req.Namespace,
		},
	}
	op, err := ctrl.CreateOrUpdate(ctx, r.Client, &cronjobScheduler, func() error {
		cronSchedule := common.GetDefaultSchedulerCronJobSchedule()
		cronjobScheduler.ObjectMeta.Labels = common.MergeLabels(
			r.makeCommonLabels(req, instancesv1alpha1.CompontentTypeScheduler),
			instance.ObjectMeta.Labels,
			extraInstanceLabels,
		)
		if instance.Spec.SchedulerCronJob.Schedule != nil {
			cronSchedule = *instance.Spec.SchedulerCronJob.Schedule
		}
		timezone := common.GetDefaultTimezone()
		if instance.Spec.Timezone != nil {
			timezone = *instance.Spec.Timezone
		}
		image := common.GetDefaultImageCurl()
		if instance.Spec.SchedulerCronJob.Image != nil {
			image = *instance.Spec.SchedulerCronJob.Image
		}
		resources := common.GetDefaultSchedulerCronJobResourceRequirements()
		cronjobScheduler.Spec = batchv1.CronJobSpec{
			Schedule: cronSchedule,
			TimeZone: ptr.To(timezone),
			JobTemplate: batchv1.JobTemplateSpec{
				Spec: batchv1.JobSpec{
					Template: v1.PodTemplateSpec{
						ObjectMeta: metav1.ObjectMeta{
							Labels: r.makeCommonLabels(req, instancesv1alpha1.CompontentTypeScheduler),
						},
						Spec: v1.PodSpec{
							RestartPolicy: v1.RestartPolicyOnFailure,
							Containers: []v1.Container{{
								Name:  "complete",
								Image: image,
								SecurityContext: &v1.SecurityContext{
									ReadOnlyRootFilesystem:   ptr.To(true),
									AllowPrivilegeEscalation: ptr.To(false),
									Capabilities: &v1.Capabilities{
										Drop: []v1.Capability{
											"ALL",
										},
									},
									SeccompProfile: &v1.SeccompProfile{
										Type: v1.SeccompProfileTypeRuntimeDefault,
									},
								},
								Resources: common.MergeResourceRequirements(&resources, instance.Spec.SchedulerCronJob.ResourcesRequirements),
								Env: []v1.EnvVar{
									{
										Name: "APP_SCHEDULER_ENDPOINT_SECRET",
										ValueFrom: &v1.EnvVarSource{
											SecretKeyRef: &v1.SecretKeySelector{
												LocalObjectReference: v1.LocalObjectReference{
													Name: serviceName,
												},
												Key: "APP_SCHEDULER_ENDPOINT_SECRET",
											},
										},
									},
								},
								Command: []string{
									"curl",
									"-v",
									"-X",
									"POST",
									"-H",
									"Accept: application/json",
									"-H",
									"X-FlatTrack-Scheduler-Secret: $(APP_SCHEDULER_ENDPOINT_SECRET)",
									serviceName + "." + req.Namespace + "/api/system/schedule",
								},
							}},
							Volumes: []v1.Volume{
								{
									Name: "tmp",
									VolumeSource: v1.VolumeSource{
										EmptyDir: &v1.EmptyDirVolumeSource{},
									},
								},
							},
						},
					},
				},
			},
		}
		cronjobScheduler.ObjectMeta.Labels = common.MergeLabels(cronjobScheduler.ObjectMeta.Labels, instance.ObjectMeta.Labels, extraInstanceLabels)
		if err := ctrl.SetControllerReference(instance, &cronjobScheduler, r.Scheme); err != nil {
			return err
		}
		if val := common.GetPodRuntimeClassName(); val != "" {
			cronjobScheduler.Spec.JobTemplate.Spec.Template.Spec.RuntimeClassName = ptr.To(val)
		}
		return nil
	})
	if err != nil {
		r.Recorder.Event(instance, v1.EventTypeWarning, string(op),
			fmt.Sprintf("%v %v/%v: %v",
				op,
				cronjobScheduler.Namespace,
				cronjobScheduler.Name, err))
		return err
	}
	if op == controllerutil.OperationResultCreated || op == controllerutil.OperationResultUpdated {
		r.Recorder.Event(instance, v1.EventTypeNormal, string(op),
			fmt.Sprintf("%v %v/%v",
				op, // TODO fix: Not Capitalised
				cronjobScheduler.Namespace,
				cronjobScheduler.Name))
	}
	return nil
}

func (r *FlatTrackReconciler) reconcileCronjobBackup(ctx context.Context, req ctrl.Request, instance *instancesv1alpha1.FlatTrack) error {
	serviceBackupName := r.getServiceBackupName(req)
	serviceName := r.getServiceName(req)
	postgresHostname := r.getPostgresHostname(req)

	cronjobBackup := batchv1.CronJob{
		ObjectMeta: metav1.ObjectMeta{
			Name:      serviceBackupName,
			Namespace: req.Namespace,
		},
	}
	if instance.Spec.BackupCronJob.Enabled != nil && !*instance.Spec.BackupCronJob.Enabled {
		cronjobBackup.ObjectMeta.Labels = common.MergeLabels(
			r.makeCommonLabels(req, instancesv1alpha1.CompontentTypeBackup),
			instance.ObjectMeta.Labels,
			extraInstanceLabels,
		)
		err := r.Get(ctx, client.ObjectKey{Namespace: req.Namespace, Name: serviceBackupName}, &cronjobBackup)
		if errors.IsNotFound(err) {
			return nil
		} else if err != nil {
			return err
		}
		if err := r.Delete(ctx, &cronjobBackup); err != nil {
			return err
		}
		return nil
	}

	op, err := ctrl.CreateOrUpdate(ctx, r.Client, &cronjobBackup, func() error {
		cronSchedule := common.GetDefaultBackupCronJobSchedule()
		if instance.Spec.BackupCronJob.Schedule != nil {
			cronSchedule = *instance.Spec.BackupCronJob.Schedule
		}
		timezone := common.GetDefaultTimezone()
		if instance.Spec.Timezone != nil {
			timezone = *instance.Spec.Timezone
		}
		imageClientPostgres := common.GetDefaultImagePostgres()
		if instance.Spec.BackupCronJob.ImageClientPostgres != nil {
			imageClientPostgres = *instance.Spec.BackupCronJob.ImageClientPostgres
		}
		imageClientMinio := common.GetDefaultImageMinio()
		if instance.Spec.BackupCronJob.ImageClientMinio != nil {
			imageClientMinio = *instance.Spec.BackupCronJob.ImageClientMinio
		}
		imageClientAge := common.GetDefaultImageAge()
		if instance.Spec.BackupCronJob.ImageClientAge != nil {
			imageClientAge = *instance.Spec.BackupCronJob.ImageClientAge
		}
		resources := common.GetDefaultBackupCronJobResourceRequirements()
		cronjobBackup.Spec = batchv1.CronJobSpec{
			Schedule: cronSchedule,
			TimeZone: ptr.To(timezone),
			JobTemplate: batchv1.JobTemplateSpec{
				Spec: batchv1.JobSpec{
					Template: v1.PodTemplateSpec{
						ObjectMeta: metav1.ObjectMeta{
							Labels: r.makeCommonLabels(req, instancesv1alpha1.CompontentTypeBackup),
						},
						Spec: v1.PodSpec{
							RestartPolicy: v1.RestartPolicyOnFailure,
							InitContainers: []v1.Container{
								{
									Name:  "backup-dump",
									Image: imageClientPostgres,
									SecurityContext: &v1.SecurityContext{
										ReadOnlyRootFilesystem:   ptr.To(true),
										AllowPrivilegeEscalation: ptr.To(false),
										Capabilities: &v1.Capabilities{
											Drop: []v1.Capability{
												"ALL",
											},
										},
										SeccompProfile: &v1.SeccompProfile{
											Type: v1.SeccompProfileTypeRuntimeDefault,
										},
									},
									Resources: common.MergeResourceRequirements(&resources, instance.Spec.BackupCronJob.ResourcesRequirements),
									EnvFrom: []v1.EnvFromSource{
										{
											SecretRef: &v1.SecretEnvSource{
												LocalObjectReference: v1.LocalObjectReference{
													Name: serviceName,
												},
											},
										},
									},
									Env: []v1.EnvVar{
										{
											Name:  "POSTGRES_HOSTNAME",
											Value: postgresHostname,
										},
									},
									Command: []string{
										"sh",
										"-c",
										"pg_dump postgresql://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@$(POSTGRES_HOSTNAME)/$(POSTGRES_DB) > /tmp/backup.sql ; " +
											"ls -alh /tmp/",
									},
									VolumeMounts: []v1.VolumeMount{{
										Name:      "tmp",
										MountPath: "/tmp",
									}},
								},
								{
									Name:  "backup-encrypt",
									Image: imageClientAge,
									SecurityContext: &v1.SecurityContext{
										ReadOnlyRootFilesystem:   ptr.To(true),
										AllowPrivilegeEscalation: ptr.To(false),
										Capabilities: &v1.Capabilities{
											Drop: []v1.Capability{
												"ALL",
											},
										},
										SeccompProfile: &v1.SeccompProfile{
											Type: v1.SeccompProfileTypeRuntimeDefault,
										},
									},
									Resources: common.MergeResourceRequirements(&resources, instance.Spec.BackupCronJob.ResourcesRequirements),
									Command: []string{
										"age", "--encrypt", "--armor", "-o", "/tmp/backup.sql.age", "-r", "$(AGE_PUBLIC_KEY)", "/tmp/backup.sql",
									},
									EnvFrom: []v1.EnvFromSource{
										{
											SecretRef: &v1.SecretEnvSource{
												LocalObjectReference: v1.LocalObjectReference{
													Name: common.GetBackupBucketSecretName(),
												},
											},
										},
									},
									VolumeMounts: []v1.VolumeMount{
										{
											Name:      "tmp",
											MountPath: "/tmp",
										},
									},
								},
								{
									Name:  "backup-push",
									Image: imageClientMinio,
									SecurityContext: &v1.SecurityContext{
										ReadOnlyRootFilesystem:   ptr.To(true),
										AllowPrivilegeEscalation: ptr.To(false),
										Capabilities: &v1.Capabilities{
											Drop: []v1.Capability{
												"ALL",
											},
										},
										SeccompProfile: &v1.SeccompProfile{
											Type: v1.SeccompProfileTypeRuntimeDefault,
										},
									},
									Resources: common.MergeResourceRequirements(&resources, instance.Spec.BackupCronJob.ResourcesRequirements),
									EnvFrom: []v1.EnvFromSource{
										{
											SecretRef: &v1.SecretEnvSource{
												LocalObjectReference: v1.LocalObjectReference{
													Name: common.GetBackupBucketSecretName(),
												},
											},
										},
									},
									Env: []v1.EnvVar{
										{
											Name:  "INSTANCE_NAME",
											Value: serviceName,
										},
									},
									Command: []string{
										"sh",
										"-c",
										"mc alias set bucket $ENDPOINT $ACCESS_KEY $SECRET_KEY && " +
											"mc cp /tmp/backup.sql.age bucket/$DESTINATION_PATH/$INSTANCE_NAME-$(date +%Y%m%d%H%M%S).sql.age",
									},
									VolumeMounts: []v1.VolumeMount{
										{
											Name:      "tmp",
											MountPath: "/root/.mc",
										},
										{
											Name:      "tmp",
											MountPath: "/tmp",
										},
									},
								},
							},
							Containers: []v1.Container{{
								Name:      "complete",
								Image:     imageClientPostgres,
								Resources: common.MergeResourceRequirements(&resources, instance.Spec.BackupCronJob.ResourcesRequirements),
								Command:   []string{"true"},
							}},
							Volumes: []v1.Volume{
								{
									Name: "tmp",
									VolumeSource: v1.VolumeSource{
										EmptyDir: &v1.EmptyDirVolumeSource{},
									},
								},
							},
						},
					},
				},
			},
		}
		cronjobBackup.ObjectMeta.Labels = common.MergeLabels(cronjobBackup.ObjectMeta.Labels, instance.ObjectMeta.Labels, extraInstanceLabels)
		if err := ctrl.SetControllerReference(instance, &cronjobBackup, r.Scheme); err != nil {
			return err
		}
		if val := common.GetPodRuntimeClassName(); val != "" {
			cronjobBackup.Spec.JobTemplate.Spec.Template.Spec.RuntimeClassName = ptr.To(val)
		}
		return nil
	})
	if err != nil {
		r.Recorder.Event(instance, v1.EventTypeWarning, string(op),
			fmt.Sprintf("%v %v/%v: %v",
				op,
				cronjobBackup.Namespace,
				cronjobBackup.Name, err))
		return err
	}
	if op == controllerutil.OperationResultCreated || op == controllerutil.OperationResultUpdated {
		r.Recorder.Event(instance, v1.EventTypeNormal, string(op),
			fmt.Sprintf("%v %v/%v",
				op, // TODO fix: Not Capitalised
				cronjobBackup.Namespace,
				cronjobBackup.Name))
	}
	return nil
}

func (r *FlatTrackReconciler) updateStatus(ctx context.Context, req ctrl.Request, instance *instancesv1alpha1.FlatTrack) error {
	log := log.FromContext(ctx)
	serviceName := r.getServiceName(req)
	postgresName := r.getPostgresName(req)
	postgresPoolerName := r.getPostgresPoolerName(req)
	serviceBackupName := r.getServiceBackupName(req)
	serviceSchedulerName := r.getServiceSchedulerName(req)

	var knsvc knservingv1.Service
	if err := r.Get(ctx, client.ObjectKey{Namespace: req.Namespace, Name: serviceName}, &knsvc); err != nil {
		return err
	}
	var pgCluster cnpgv1.Cluster
	if err := r.Get(ctx, client.ObjectKey{Namespace: req.Namespace, Name: postgresName}, &pgCluster); err != nil {
		return err
	}
	var pgPooler cnpgv1.Pooler
	if err := r.Get(ctx, client.ObjectKey{Namespace: req.Namespace, Name: postgresPoolerName}, &pgPooler); err != nil {
		return err
	}

	var cronjobBackup batchv1.CronJob
	if instance.Spec.BackupCronJob.Enabled == nil || (instance.Spec.BackupCronJob.Enabled != nil && *instance.Spec.BackupCronJob.Enabled) {
		if err := r.Get(ctx, client.ObjectKey{Namespace: req.Namespace, Name: serviceBackupName}, &cronjobBackup); err != nil {
			return err
		}
	}

	var cronjobScheduler batchv1.CronJob
	if err := r.Get(ctx, client.ObjectKey{Namespace: req.Namespace, Name: serviceSchedulerName}, &cronjobScheduler); err != nil {
		return err
	}

	var knsvcReady bool
	for _, c := range knsvc.Status.Conditions {
		if c.Type == "Ready" && c.Status == "True" {
			knsvcReady = true
			break
		}
	}
	pgClusterReady := pgCluster.Status.ReadyInstances == pgCluster.Spec.Instances
	pgPoolerReady := pgPooler.Status.Instances == *pgPooler.Spec.Instances

	if instance.Status.Registered == nil {
		instance.Status.Registered = ptr.To(false)
	}
	instance.Status.URL = ptr.To(knsvc.Status.URL.String())
	instance.Status.Ready = ptr.To(pgClusterReady && pgPoolerReady && knsvcReady)
	instance.Status.Images = []instancesv1alpha1.Image{
		{Name: knsvc.Spec.ConfigurationSpec.Template.Spec.PodSpec.Containers[0].Image, For: "flattrack"},
		{Name: pgCluster.Status.Image, For: "postgres"},
		{Name: cronjobScheduler.Spec.JobTemplate.Spec.Template.Spec.Containers[0].Image, For: "schedulercronjob"},
	}
	if instance.Spec.BackupCronJob.Enabled == nil || (instance.Spec.BackupCronJob.Enabled != nil && *instance.Spec.BackupCronJob.Enabled) {
		for _, c := range cronjobBackup.Spec.JobTemplate.Spec.Template.Spec.InitContainers {
			instance.Status.Images = append(instance.Status.Images, instancesv1alpha1.Image{Name: c.Image, For: "backupcronjob"})
		}
	}
	instance.Status.LastBackupTime = cronjobBackup.Status.LastSuccessfulTime
	instance.Status.LastSchedulerTime = cronjobScheduler.Status.LastSuccessfulTime
	instance.Status.ObservedGeneration = instance.ObjectMeta.Generation

	var pgsecForPass v1.Secret
	if err := r.Get(ctx, client.ObjectKey{Namespace: req.Namespace, Name: serviceName}, &pgsecForPass); err != nil {
		return err
	}
	registrationSecret, ok := pgsecForPass.Data["APP_REGISTRATION_SECRET"]
	if !ok {
		return fmt.Errorf("missing APP_REGISTRATION_SECRET key in pgsec")
	}
	// NOTE so it can work locally, although this field doesn't get updated
	//      due to running on the localhost, it doesn't have access to the
	//      network addresses inside of a cluster
	init, err := common.GetInstanceInitialized(knsvc.Status.URL.String())
	if err != nil {
		return err
	}
	if init != nil {
		instance.Status.Registered = ptr.To(*init)
		if instance.Status.URL != nil && !*init {
			instance.Status.RegistrationURL, err = makeRegistrationURL(ctx, instance, string(registrationSecret))
			if err != nil {
				return err
			}
		} else {
			instance.Status.RegistrationURL = nil
		}
	} else {
		log.Info("instance " + req.Name + " not yet initialised")
	}

	if err := r.Status().Update(ctx, instance); err != nil {
		return err
	}
	return nil
}

//+kubebuilder:rbac:groups=instances.flattrack.io,resources=flattracks,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=instances.flattrack.io,resources=flattracks/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=instances.flattrack.io,resources=flattracks/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=persistentvolumeclaims,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="",resources=services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="",resources=events,verbs=create;patch
//+kubebuilder:rbac:groups="apps",resources=statefulsets,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="batch",resources=cronjobs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=serving.knative.dev,resources=services,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the FlatTrack object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.16.3/pkg/reconcile
func (r *FlatTrackReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := log.FromContext(ctx)

	log.Info("reconciling FlatTrack instance...")

	instance := &instancesv1alpha1.FlatTrack{}
	if err := r.Get(context.TODO(), req.NamespacedName, instance); err != nil {
		if errors.IsNotFound(err) {
			// object not found, could have been deleted after
			// reconcile request, hence don't requeue
			return ctrl.Result{}, nil
		}

		// error reading the object, requeue the request
		return ctrl.Result{}, err
	}

	if instance.ObjectMeta.DeletionTimestamp.IsZero() {
		if !controllerutil.ContainsFinalizer(instance, finalizerName) {
			_ = controllerutil.AddFinalizer(instance, finalizerName)
			if err := r.Update(ctx, instance); err != nil {
				return ctrl.Result{}, err
			}
		}
	} else {
		if controllerutil.ContainsFinalizer(instance, finalizerName) {
			if err := r.deleteResources(ctx, req); err != nil {
				return ctrl.Result{}, err
			}
			_ = controllerutil.RemoveFinalizer(instance, finalizerName)
			if err := r.Update(ctx, instance); err != nil {
				return ctrl.Result{}, err
			}
		}
		return ctrl.Result{}, nil
	}

	log.Info("reconciling Secret")
	if err := r.reconcileSecret(ctx, req, instance); err != nil {
		log.Error(err, "reconcile secret")
		return ctrl.Result{Requeue: true}, nil
	}

	log.Info("reconciling Postgres")
	res, err := r.reconcilePostgres(ctx, req, instance)
	if err != nil {
		log.Error(err, "reconcile Postgres Cluster")
		return ctrl.Result{Requeue: true}, nil
	}
	if !res.IsZero() {
		return res, nil
	}

	log.Info("reconciling Postgres Pooler")
	res, err = r.reconcilePostgresPooler(ctx, req, instance)
	if err != nil {
		log.Error(err, "reconcile Postgres Pooler")
		return ctrl.Result{Requeue: true}, nil
	}
	if !res.IsZero() {
		return res, nil
	}

	// NOTE conditions
	//        - if instance is set to restore from a backup
	//        - if can connect to instance db
	//        - if instance is not already registered
	if instance.Spec.RestoreFromBackup != nil &&
		*instance.Spec.RestoreFromBackup {
		log.Info("waiting to restore from backup")
		return ctrl.Result{Requeue: true}, nil
	}
	log.Info("reconciling Knative Service")
	if err := r.reconcileKnService(ctx, req, instance); err != nil {
		log.Error(err, "reconcile Knative Service")
		return ctrl.Result{Requeue: true}, nil
	}
	log.Info("reconciling CronJob Scheduler")
	if err := r.reconcileCronjobScheduler(ctx, req, instance); err != nil {
		log.Error(err, "reconcile CronJob Scheduler")
		return ctrl.Result{Requeue: true}, nil
	}
	log.Info("reconciling CronJob Backup")
	if err := r.reconcileCronjobBackup(ctx, req, instance); err != nil {
		log.Error(err, "reconcile CronJob Backup")
		return ctrl.Result{Requeue: true}, nil
	}
	log.Info("reconciling Status")
	if err := r.updateStatus(ctx, req, instance); err != nil {
		log.Error(err, "reconcile status")
		return ctrl.Result{}, err
	}

	if (instance.Status.Ready != nil && !*instance.Status.Ready) ||
		(instance.Status.Registered != nil && !*instance.Status.Registered) {
		log.Info("instance either not ready or registered")
		return ctrl.Result{RequeueAfter: 1 * time.Second}, nil
	}

	return ctrl.Result{}, nil
}

func (r *FlatTrackReconciler) deleteResources(ctx context.Context, req ctrl.Request) error {
	log := log.FromContext(ctx)
	log.Info("deleting resources for " + req.Namespace + "/" + req.Name)
	var serviceName = r.getServiceName(req)
	var postgresName = r.getPostgresName(req)
	var serviceBackupName = r.getServiceBackupName(req)
	var serviceSchedulerName = r.getServiceSchedulerName(req)
	var errs []error
	var wg sync.WaitGroup
	var mu sync.Mutex

	resources := []client.Object{
		&v1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      serviceName,
				Namespace: req.Namespace,
			},
		},
		&cnpgv1.Cluster{
			ObjectMeta: metav1.ObjectMeta{
				Name:      postgresName,
				Namespace: req.Namespace,
			},
		},
		&cnpgv1.Pooler{
			ObjectMeta: metav1.ObjectMeta{
				Name:      postgresName,
				Namespace: req.Namespace,
			},
		},
		&batchv1.CronJob{
			ObjectMeta: metav1.ObjectMeta{
				Name:      serviceBackupName,
				Namespace: req.Namespace,
			},
		},
		&batchv1.CronJob{
			ObjectMeta: metav1.ObjectMeta{
				Name:      serviceSchedulerName,
				Namespace: req.Namespace,
			},
		},
		&knservingv1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name:      serviceName,
				Namespace: req.Namespace,
			},
		},
	}
	for _, o := range resources {
		o := o
		wg.Add(1)
		go func(o client.Object) {
			// TODO better way of getting type name, since o.GetObjectKind().GroupVersionKind().Kind doesn't contain anything
			log.Info("deleting: " + reflect.TypeOf(o).Elem().Name() + "/" + o.GetNamespace() + "/" + o.GetName())
			if err := r.Delete(ctx, o); err != nil && !errors.IsNotFound(err) {
				mu.Lock()
				errs = append(errs, err)
				mu.Unlock()
			}
			wg.Done()
		}(o)
	}
	wg.Wait()
	if len(errs) > 0 {
		return fmt.Errorf("errors: %v", errs)
	}
	return nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *FlatTrackReconciler) SetupWithManager(mgr ctrl.Manager) error {
	extraInstanceLabels = common.GetExtraInstanceLabels()
	return ctrl.NewControllerManagedBy(mgr).
		For(&instancesv1alpha1.FlatTrack{}).
		Complete(r)
}
