/*

Copyright 2024 Flattrack authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"net/url"
	"path/filepath"
	"runtime"
	"slices"
	"strings"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"filippo.io/age"
	cnpgv1 "github.com/cloudnative-pg/cloudnative-pg/api/v1"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/utils/ptr"
	knscheme "knative.dev/serving/pkg/client/clientset/versioned/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/envtest"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"gitlab.com/flattrack/operator/api/v1alpha1"
	instancesv1alpha1 "gitlab.com/flattrack/operator/api/v1alpha1"
	"gitlab.com/flattrack/operator/internal/common"
	//+kubebuilder:scaffold:imports
)

// These tests use Ginkgo (BDD-style Go testing framework). Refer to
// http://onsi.github.io/ginkgo/ to learn more about Ginkgo.

const (
	defaultTimeout = 7 * time.Minute
)

const (
	minioAccessKey = "ft-minio123"
	minioSecretKey = "ft-minio123"
	minioPath      = "app-flattrack-io"
)

var cfg *rest.Config
var k8sClient client.Client
var testEnv *envtest.Environment
var ctx context.Context
var cancel context.CancelFunc
var minioClient *minio.Client
var agePublicKey string
var minioEndpoint = common.GetEnvOrDefault("OPERATOR_TEST_MINIO_ENDPOINT", "localhost:9000")

func getBackupName(i string) string {
	return i + "-backup"
}

func TestControllers(t *testing.T) {
	RegisterFailHandler(Fail)

	RunSpecs(t, "Controller Suite")
}

var _ = BeforeSuite(func() {
	logf.SetLogger(zap.New(zap.WriteTo(GinkgoWriter), zap.UseDevMode(true)))

	ctx, cancel = context.WithCancel(context.TODO())

	By("bootstrapping test environment")
	testEnv = &envtest.Environment{
		CRDDirectoryPaths:     []string{filepath.Join("..", "..", "config", "crd", "bases")},
		ErrorIfCRDPathMissing: true,
		UseExistingCluster:    ptr.To(true),

		// The BinaryAssetsDirectory is only required if you want to run the tests directly
		// without call the makefile target test. If not informed it will look for the
		// default path defined in controller-runtime which is /usr/local/kubebuilder/.
		// Note that you must have the required binaries setup under the bin directory to perform
		// the tests directly. When we run make test it will be setup and used automatically.
		BinaryAssetsDirectory: filepath.Join("..", "..", "bin", "k8s",
			fmt.Sprintf("1.28.3-%s-%s", runtime.GOOS, runtime.GOARCH)),
	}

	var err error
	// cfg is defined in this file globally.
	cfg, err = testEnv.Start()
	Expect(err).NotTo(HaveOccurred())
	Expect(cfg).NotTo(BeNil())

	err = instancesv1alpha1.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	err = knscheme.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())

	err = cnpgv1.AddToScheme(scheme.Scheme)
	Expect(err).NotTo(HaveOccurred())
	//+kubebuilder:scaffold:scheme

	k8sClient, err = client.New(cfg, client.Options{Scheme: scheme.Scheme})
	Expect(err).NotTo(HaveOccurred())
	Expect(k8sClient).NotTo(BeNil())

	k8sManager, err := ctrl.NewManager(cfg, ctrl.Options{
		Scheme: scheme.Scheme,
	})
	Expect(err).ToNot(HaveOccurred())
	err = (&FlatTrackReconciler{
		Client:   k8sManager.GetClient(),
		Scheme:   k8sManager.GetScheme(),
		Recorder: k8sManager.GetEventRecorderFor("flattrack"),
	}).SetupWithManager(k8sManager)
	Expect(err).ToNot(HaveOccurred())

	go func() {
		defer GinkgoRecover()
		err = k8sManager.Start(ctx)
		Expect(err).ToNot(HaveOccurred(), "failed to run manager")
	}()

	identity, err := age.GenerateX25519Identity()
	Expect(err).To(Succeed(), "failed to generate age key")
	agePublicKey = identity.Recipient().String()

	mc, err := minio.New(minioEndpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(minioAccessKey, minioSecretKey, ""),
		Secure: false,
	})
	Expect(err).To(Succeed(), "failed to create Minio Client")
	minioClient = mc
})

var _ = AfterSuite(func() {
	cancel()
	By("tearing down the test environment")
	err := testEnv.Stop()
	Expect(err).NotTo(HaveOccurred())
})

var _ = Describe("FlatTrack controller", func() {
	var testNamespace string
	BeforeEach(func() {
		ctx := context.Background()
		testNamespace = strings.ToLower(fmt.Sprintf("flattrack-operator-test-%v-%v", GinkgoParallelProcess(), common.RandString(5)))
		Expect(k8sClient.Create(ctx, &v1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: testNamespace,
				Labels: map[string]string{
					"operator.flattrack.io/test-namespace": "true",
				},
			},
		})).Should(Succeed())
		Expect(k8sClient.Create(ctx, &v1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "backup-bucket",
				Namespace: testNamespace,
			},
			StringData: map[string]string{
				"ACCESS_KEY":       minioAccessKey,
				"SECRET_KEY":       minioSecretKey,
				"ENDPOINT":         "http://minio.minio:9000/",
				"DESTINATION_PATH": minioPath,
				"AGE_PUBLIC_KEY":   agePublicKey,
			},
		})).Should(Succeed())
	})
	AfterEach(func() {
		ctx := context.Background()
		Expect(k8sClient.Delete(ctx, &v1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: testNamespace,
			},
		})).Should(Succeed())
	})

	It("should run the lifecycle of a FlatTrack instance", func() {
		By("creating a FlatTrack instance")
		ctx := context.Background()
		testLabelKey := strings.ToLower(common.RandString(8))
		ft := v1alpha1.FlatTrack{
			ObjectMeta: metav1.ObjectMeta{
				Name:      strings.ToLower(common.RandString(8)) + "-lifecycle",
				Namespace: testNamespace,
				Labels: map[string]string{
					testLabelKey: "yes",
				},
			},
		}
		Expect(k8sClient.Create(ctx, &ft)).Should(Succeed(), "failed to create instance")

		requirement, err := labels.NewRequirement(testLabelKey, selection.Equals, []string{"yes"})
		Expect(err).To(BeNil(), "failed to create new requirement")
		ls := labels.NewSelector().Add(*requirement)
		By("expecting to see a single instance")
		var list v1alpha1.FlatTrackList
		Expect(k8sClient.List(ctx, &list, &client.ListOptions{Namespace: testNamespace, LabelSelector: ls})).Should(BeNil(), "failed to list instances")
		Expect(len(list.Items)).Should(Equal(1))

		By("waiting for instance to be ready")
		Eventually(func() (bool, error) {
			if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft); err != nil {
				return false, err
			}
			if ft.Status.Ready == nil {
				return false, fmt.Errorf("instance not ready yet")
			}
			return *ft.Status.Ready, nil
		}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find status ready")

		Expect(len(ft.Status.Images) >= 5).To(BeTrue(), "failed to find at least five images")
		Expect(ft.Status.URL).NotTo(BeNil(), "failed to find URL")
		_, err = url.Parse(*ft.Status.URL)
		Expect(err).Should(Succeed(), "failed to parse URL")

		Expect(ft.Status.RegistrationURL).NotTo(BeNil(), "failed to find URL")
		_, err = url.Parse(*ft.Status.URL)
		Expect(err).Should(Succeed(), "failed to parse URL")

		imageFlatTrack := "registry.gitlab.com/flattrack/flattrack@sha256:ec398500261a2db0c159436a4a1e93105d9ba110ce751b57762366e2317a7910"
		imagePostgres := "ghcr.io/cloudnative-pg/postgresql:16.4@sha256:9447bc8a82997990a70cfae710ee3a5fab4bf62f5122c06cc7b74945a2dc09df"
		ft.Spec.Image = ptr.To(imageFlatTrack)
		ft.Spec.Postgres.Image = ptr.To(imagePostgres)
		Expect(k8sClient.Update(ctx, &ft)).To(Succeed(), "failed to update instance")

		By("waiting for instance to be ready")
		Eventually(func() (bool, error) {
			if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft); err != nil {
				return false, err
			}
			if ft.Status.Ready == nil {
				return false, fmt.Errorf("instance not ready yet")
			}
			i := slices.IndexFunc(ft.Status.Images, func(i v1alpha1.Image) bool {
				return i.For == "flattrack"
			})
			if ft.Status.Images[i].Name != imageFlatTrack {
				return false, fmt.Errorf("image doesn't match updated image")
			}
			i = slices.IndexFunc(ft.Status.Images, func(i v1alpha1.Image) bool {
				return i.For == "postgres"
			})
			if ft.Status.Images[i].Name != imagePostgres {
				return false, fmt.Errorf("postgres image doesn't match updated image")
			}
			return *ft.Status.Ready, nil
		}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find status ready")

		By("deleting the instance")
		Expect(k8sClient.Delete(ctx, &ft)).Should(BeNil(), "failed to delete instance")

		By("expecting the instance to be deleted")
		Eventually(func() (bool, error) {
			err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft)
			if errors.IsNotFound(err) {
				return true, nil
			}
			if err != nil {
				return false, err
			}
			return false, nil
		}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find status ready")
	})

	It("should have a URL populated", func() {
		By("Creating a FlatTrack instance")
		ctx := context.Background()
		ft := v1alpha1.FlatTrack{
			ObjectMeta: metav1.ObjectMeta{
				Name:      strings.ToLower(common.RandString(8)) + "-url-populated",
				Namespace: testNamespace,
			},
		}
		Expect(k8sClient.Create(ctx, &ft)).Should(Succeed(), "failed to create instance")

		By("waiting for the url field to be populated")
		Eventually(func() (*string, error) {
			if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft); err != nil {
				return nil, err
			}
			if ft.Status.URL == nil {
				return nil, fmt.Errorf("no url populated")
			}
			return ft.Status.URL, nil
		}, defaultTimeout, 2*time.Second).Should(Not(BeNil()), "failed to find populated URL in status")

		Expect(k8sClient.Delete(ctx, &ft)).Should(Succeed(), "failed to delete instance")
	})

	It("should have a registrationURL populated", func() {
		By("creating a FlatTrack instance")
		ctx := context.Background()
		ft := v1alpha1.FlatTrack{
			ObjectMeta: metav1.ObjectMeta{
				Name:      strings.ToLower(common.RandString(8)) + "-registrationurl-populated",
				Namespace: testNamespace,
			},
		}
		Expect(k8sClient.Create(ctx, &ft)).Should(Succeed(), "failed to create instance")

		By("waiting for the registrationURL to be deleted")
		Eventually(func() (*string, error) {
			if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft); err != nil {
				return nil, err
			}
			if ft.Status.RegistrationURL == nil {
				return nil, fmt.Errorf("no url populated")
			}
			return ft.Status.RegistrationURL, nil
		}, defaultTimeout, 2*time.Second).Should(Not(BeNil()), "failed to find populated registrationURL in status")

		Expect(ft.Status.RegistrationURL).NotTo(BeNil(), "registrationURL is nil")

		By("validating registration url")
		u, err := url.Parse(*ft.Status.RegistrationURL)
		Expect(err).To(Succeed())
		Expect(u.Path).To(Equal("/setup"))
		Expect(u.Query().Get("secret")).ToNot(BeNil())

		Expect(k8sClient.Delete(ctx, &ft)).Should(Succeed(), "failed to delete instance")
	})

	Context("when configuring scheduler", func() {
		It("schedules a scheduler job accordingly", func() {
			By("creating a FlatTrack instance")
			ctx := context.Background()
			ft := v1alpha1.FlatTrack{
				ObjectMeta: metav1.ObjectMeta{
					Name:      strings.ToLower(common.RandString(8)) + "-scheduler-schedules",
					Namespace: testNamespace,
				},
				Spec: instancesv1alpha1.FlatTrackSpec{
					Timezone: ptr.To("Pacific/Auckland"),
					SchedulerCronJob: instancesv1alpha1.SchedulerCronJob{
						Schedule: ptr.To("* * * * *"),
					},
				},
			}
			Expect(k8sClient.Create(ctx, &ft)).Should(Succeed(), "failed to create instance")

			By("waiting for instance to be ready")
			Eventually(func() (bool, error) {
				if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft); err != nil {
					return false, err
				}
				if ft.Status.Ready == nil {
					return false, fmt.Errorf("instance not ready yet")
				}
				return *ft.Status.Ready, nil
			}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find status ready")

			requirement, err := labels.NewRequirement("operator.flattrack.io/instance", selection.Equals, []string{ft.ObjectMeta.Name})
			Expect(err).To(BeNil(), "failed to create new requirement")
			ls := labels.NewSelector().Add(*requirement)
			var jl batchv1.JobList

			Eventually(func() (bool, error) {
				if err := k8sClient.List(ctx, &jl, &client.ListOptions{Namespace: testNamespace, LabelSelector: ls}); err != nil {
					return false, err
				}
				if len(jl.Items) == 0 {
					return false, fmt.Errorf("failed to find a job created")
				}
				return true, nil
			}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find a job created")

			Expect(k8sClient.Delete(ctx, &ft)).Should(Succeed(), "failed to delete instance")
		})
	})

	Context("when configuring backup", func() {
		It("schedules a backup job accordingly", func() {
			By("creating a FlatTrack instance")
			ctx := context.Background()
			ft := v1alpha1.FlatTrack{
				ObjectMeta: metav1.ObjectMeta{
					Name:      strings.ToLower(common.RandString(8)) + "-backup-schedules",
					Namespace: testNamespace,
				},
				Spec: instancesv1alpha1.FlatTrackSpec{
					Timezone: ptr.To("Pacific/Auckland"),
					BackupCronJob: instancesv1alpha1.BackupCronJob{
						Schedule: ptr.To("* * * * *"),
					},
				},
			}
			Expect(k8sClient.Create(ctx, &ft)).Should(Succeed(), "failed to create instance")

			By("waiting for instance to be ready")
			Eventually(func() (bool, error) {
				if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft); err != nil {
					return false, err
				}
				if ft.Status.Ready == nil {
					return false, fmt.Errorf("instance not ready yet")
				}
				return *ft.Status.Ready, nil
			}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find status ready")

			requirement, err := labels.NewRequirement("operator.flattrack.io/instance", selection.Equals, []string{ft.ObjectMeta.Name})
			Expect(err).To(BeNil(), "failed to create new requirement")
			ls := labels.NewSelector().Add(*requirement)
			var jl batchv1.JobList

			Eventually(func() (bool, error) {
				if err := k8sClient.List(ctx, &jl, &client.ListOptions{Namespace: testNamespace, LabelSelector: ls}); err != nil {
					return false, err
				}
				if len(jl.Items) == 0 {
					return false, fmt.Errorf("failed to find a job created")
				}
				return true, nil
			}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find a job created")

			Eventually(func() (bool, error) {
				var job batchv1.Job
				if err := k8sClient.Get(ctx, client.ObjectKey{Name: jl.Items[0].ObjectMeta.Name, Namespace: jl.Items[0].ObjectMeta.Namespace}, &job); err != nil {
					return false, err
				}
				if job.Status.Succeeded == 0 {
					return false, fmt.Errorf("job not succeeded")
				}
				return true, nil
			}, defaultTimeout, 2*time.Second).Should(BeTrue(), "")

			foundKey := ""
			Eventually(func() (bool, error) {
				for obj := range minioClient.ListObjects(context.Background(), minioPath, minio.ListObjectsOptions{}) {
					if strings.Contains(obj.Key, ft.ObjectMeta.Name) {
						foundKey = obj.Key
						break
					}
				}
				if foundKey == "" {
					return false, fmt.Errorf("object not found yet")
				}
				return true, nil
			}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find back up file")
			Expect(minioClient.RemoveObject(context.Background(), minioPath, foundKey, minio.RemoveObjectOptions{})).To(Succeed(), "failed to delete back up file")

			Expect(k8sClient.Delete(ctx, &ft)).Should(Succeed(), "failed to delete instance")
		})

		It("should not create cronjob when not enabled", func() {
			By("creating a FlatTrack instance")
			ctx := context.Background()
			ft := v1alpha1.FlatTrack{
				ObjectMeta: metav1.ObjectMeta{
					Name:      strings.ToLower(common.RandString(8)) + "-cronjob-not-enabled",
					Namespace: testNamespace,
				},
				Spec: instancesv1alpha1.FlatTrackSpec{
					BackupCronJob: instancesv1alpha1.BackupCronJob{
						Enabled: ptr.To(false),
					},
				},
			}
			Expect(k8sClient.Create(ctx, &ft)).Should(Succeed(), "failed to create instance")

			By("waiting for instance to be ready")
			Eventually(func() (bool, error) {
				if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft); err != nil {
					return false, err
				}
				if ft.Status.Ready == nil {
					return false, fmt.Errorf("instance not ready yet")
				}
				return *ft.Status.Ready, nil
			}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find status ready")

			By("expecting not found for cronjob resource")
			var cronjob batchv1.CronJob
			err := k8sClient.Get(ctx, client.ObjectKey{Name: getBackupName(ft.ObjectMeta.Name), Namespace: ft.ObjectMeta.Namespace}, &cronjob)
			Expect(errors.IsNotFound(err)).To(BeTrue())

			Expect(k8sClient.Delete(ctx, &ft)).Should(Succeed(), "failed to delete instance")
		})

		It("should delete backup cronjob after disabling the setting", func() {
			ctx := context.Background()
			By("creating an instance with backups enabled")
			ft := v1alpha1.FlatTrack{
				ObjectMeta: metav1.ObjectMeta{
					Name:      strings.ToLower(common.RandString(8)) + "-update-no-backup",
					Namespace: testNamespace,
				},
			}
			Expect(k8sClient.Create(ctx, &ft)).Should(Succeed(), "failed to create instance")

			By("waiting for instance to be ready")
			Eventually(func() (bool, error) {
				if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft); err != nil {
					return false, err
				}
				if ft.Status.Ready == nil {
					return false, fmt.Errorf("instance not ready yet")
				}
				return *ft.Status.Ready, nil
			}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find status ready")

			By("ensuring that backup cronjob exists")
			var cronjob batchv1.CronJob
			Expect(k8sClient.Get(ctx, client.ObjectKey{Name: getBackupName(ft.ObjectMeta.Name), Namespace: ft.ObjectMeta.Namespace}, &cronjob)).To(Succeed())

			By("disabling backup cronjob")
			ft.Spec.BackupCronJob.Enabled = ptr.To(false)
			Expect(k8sClient.Update(ctx, &ft)).Should(Succeed(), "failed to update instance")

			By("ensuring that backup cronjob doesn't exist")
			Eventually(func() (bool, error) {
				err := k8sClient.Get(ctx, client.ObjectKey{Name: getBackupName(ft.ObjectMeta.Name), Namespace: ft.ObjectMeta.Namespace}, &cronjob)
				if errors.IsNotFound(err) {
					return true, nil
				}
				if err != nil {
					return false, err
				}
				return true, nil
			}, defaultTimeout, 2*time.Second).Should(Not(BeNil()), "should not find instance after deletion")

			By("waiting for cronjob images in status to be nil")
			Eventually(func() (bool, error) {
				if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft); err != nil {
					return false, err
				}
				found := false
				for _, i := range ft.Status.Images {
					if i.For == "backupcronjob" {
						found = true
					}
				}
				if !found {
					return false, fmt.Errorf("images still in status")
				}
				return true, nil
			}, defaultTimeout, 2*time.Second).Should(BeTrue(), "images still in status")

			Expect(k8sClient.Delete(ctx, &ft)).Should(Succeed(), "failed to delete instance")
		})
	})

	Context("when postgres persistent volume size is set", func() {
		It("the volume size is set", func() {
			ctx := context.Background()
			ft := v1alpha1.FlatTrack{
				ObjectMeta: metav1.ObjectMeta{
					Name:      strings.ToLower(common.RandString(8)) + "-pg-pvc",
					Namespace: testNamespace,
				},
				Spec: instancesv1alpha1.FlatTrackSpec{
					Postgres: instancesv1alpha1.Postgres{
						PersistentVolumeSize: ptr.To("100Mi"),
					},
				},
			}
			Expect(k8sClient.Create(ctx, &ft)).Should(Succeed(), "failed to create instance")

			var pgpvc v1.PersistentVolumeClaim
			Eventually(func() (bool, error) {
				if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name + "-postgres-1", Namespace: ft.ObjectMeta.Namespace}, &pgpvc); err != nil {
					return false, err
				}
				return true, nil
			}, defaultTimeout, 2*time.Second).Should(Not(BeNil()), "failed to find persistent volume claim")
			Expect(pgpvc.Spec.Resources.Requests[v1.ResourceStorage].Equal(resource.MustParse("100Mi"))).To(BeTrue())

			Expect(k8sClient.Delete(ctx, &ft)).Should(Succeed(), "failed to delete instance")
		})
	})

	Context("when registering an instance", func() {
		It("should have registered set to true", func() {
			ctx := context.Background()
			ft := v1alpha1.FlatTrack{
				ObjectMeta: metav1.ObjectMeta{
					Name:      strings.ToLower(common.RandString(8)) + "-registration-set-registered",
					Namespace: testNamespace,
				},
			}
			Expect(k8sClient.Create(ctx, &ft)).Should(Succeed(), "failed to create instance")

			By("waiting for instance to be ready")
			Eventually(func() (bool, error) {
				if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft); err != nil {
					return false, err
				}
				if ft.Status.Ready == nil {
					return false, fmt.Errorf("instance not ready yet")
				}
				return *ft.Status.Ready, nil
			}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find status ready")

			By("registering instance")
			jwt, err := common.PostRegisterInstance(*ft.Status.RegistrationURL, common.Registration{
				Timezone: "Pacific/Auckland",
				Language: "English",
				FlatName: "cool",
				User: common.UserSpec{
					Names:    "John Smith",
					Email:    "john@example.com",
					Password: "Really-G00d-Password()",
				},
			})

			Expect(err).To(Succeed())
			Expect(jwt).ToNot(BeNil(), "jwt must not be nil")

			By("waiting for instance to be registered")
			Eventually(func() (bool, error) {
				if err := k8sClient.Get(ctx, client.ObjectKey{Name: ft.ObjectMeta.Name, Namespace: ft.ObjectMeta.Namespace}, &ft); err != nil {
					return false, err
				}
				if ft.Status.Registered == nil {
					return false, fmt.Errorf("instance not registered yet")
				}
				return ft.Status.Registered != nil && *ft.Status.Registered, nil
			}, defaultTimeout, 2*time.Second).Should(BeTrue(), "failed to find status registered")

			Expect(k8sClient.Delete(ctx, &ft)).Should(Succeed(), "failed to delete instance")
		})
	})

	// TODO add test for backup complete
	// TODO add test for scheduler schedule ran
})
