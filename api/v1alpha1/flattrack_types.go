/*
Copyright 2024 Flattrack authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

type ComponentType string

const (
	CompontentTypeFlatTrack ComponentType = "flattrack"
	CompontentTypePostgres  ComponentType = "postgres"
	CompontentTypeScheduler ComponentType = "scheduler"
	CompontentTypeBackup    ComponentType = "backup"
)

type Pooler struct {
	Instances             *int                     `json:"instances,omitempty"`
	ResourcesRequirements *v1.ResourceRequirements `json:"resources,omitempty"`
}

type Postgres struct {
	Instances             *int                     `json:"instances,omitempty"`
	Pooler                Pooler                   `json:"pooler,omitempty"`
	Image                 *string                  `json:"image,omitempty"`
	PersistentVolumeSize  *string                  `json:"persistentVolumeSize,omitempty"`
	ResourcesRequirements *v1.ResourceRequirements `json:"resources,omitempty"`
}

type BackupCronJob struct {
	Enabled               *bool                    `json:"enabled,omitempty"`
	ImageClientPostgres   *string                  `json:"imageClientPostgres,omitempty"`
	ImageClientMinio      *string                  `json:"imageClientMinio,omitempty"`
	ImageClientAge        *string                  `json:"imageClientAge,omitempty"`
	Schedule              *string                  `json:"schedule,omitempty"`
	ResourcesRequirements *v1.ResourceRequirements `json:"resources,omitempty"`
}

type SchedulerCronJob struct {
	Image                 *string                  `json:"image,omitempty"`
	Schedule              *string                  `json:"schedule,omitempty"`
	ResourcesRequirements *v1.ResourceRequirements `json:"resources,omitempty"`
}

type Image struct {
	Name string `json:"image"`
	For  string `json:"for"`
}

// FlatTrackSpec defines the desired state of FlatTrack
type FlatTrackSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	Image                 *string                  `json:"image,omitempty"`
	ServiceAnnotations    map[string]string        `json:"serviceAnnotations,omitempty"`
	Postgres              Postgres                 `json:"postgres,omitempty"`
	BackupCronJob         BackupCronJob            `json:"backupCronJob,omitempty"`
	SchedulerCronJob      SchedulerCronJob         `json:"schedulerCronJob,omitempty"`
	RestoreFromBackup     *bool                    `json:"restoreFromBackup,omitempty"`
	ResourcesRequirements *v1.ResourceRequirements `json:"resources,omitempty"`
	Timezone              *string                  `json:"timezone,omitempty"`
}

// FlatTrackStatus defines the observed state of FlatTrack
type FlatTrackStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	Images                    []Image      `json:"images,omitempty"`
	Registered                *bool        `json:"registered,omitempty"`
	Ready                     *bool        `json:"ready,omitempty"`
	URL                       *string      `json:"url,omitempty"`
	RestoreFromBackupComplete *bool        `json:"restoreFromBackupComplete,omitempty"`
	LastBackupTime            *metav1.Time `json:"lastBackupTime,omitempty"`
	LastSchedulerTime         *metav1.Time `json:"lastSchedulerTime,omitempty"`
	RegistrationURL           *string      `json:"registrationURL,omitempty"`
	ObservedGeneration        int64        `json:"observedGeneration,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:printcolumn:name="URL",type=string,JSONPath=`.status.url`
//+kubebuilder:printcolumn:name="Ready",type=string,JSONPath=`.status.ready`

// FlatTrack is the Schema for the flattracks API
type FlatTrack struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   FlatTrackSpec   `json:"spec,omitempty"`
	Status FlatTrackStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// FlatTrackList contains a list of FlatTrack
type FlatTrackList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []FlatTrack `json:"items"`
}

func init() {
	SchemeBuilder.Register(&FlatTrack{}, &FlatTrackList{})
}
