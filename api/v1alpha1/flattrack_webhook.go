/*
Copyright 2024 Flattrack authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"reflect"
	"sync"

	"github.com/google/go-containerregistry/pkg/crane"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
	ctrl "sigs.k8s.io/controller-runtime"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
	"sigs.k8s.io/controller-runtime/pkg/webhook/admission"
)

// log is for logging in this package.
var flattracklog = logf.Log.WithName("flattrack-resource")

// SetupWebhookWithManager will setup the manager to manage the webhooks
func (r *FlatTrack) SetupWebhookWithManager(mgr ctrl.Manager) error {
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

//+kubebuilder:webhook:path=/mutate-instances-flattrack-io-v1alpha1-flattrack,mutating=true,failurePolicy=fail,sideEffects=None,groups=instances.flattrack.io,resources=flattracks,verbs=create;update,versions=v1alpha1,name=mflattrack.kb.io,admissionReviewVersions=v1

var _ webhook.Defaulter = &FlatTrack{}

type test struct {
	value     any
	fieldPath *field.Path
	fn        func() error
}

func (r *FlatTrack) validateField(wg *sync.WaitGroup, mu *sync.Mutex, errs field.ErrorList, value any, fieldPath *field.Path, fn func() error) {
	val := reflect.ValueOf(value)
	if val.IsZero() || val.IsNil() {
		return
	}
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	wg.Add(1)
	flattracklog.Info("validating " + fieldPath.String() + " for instance " + r.Namespace + "/" + r.Name)
	if err := fn(); err != nil {
		mu.Lock()
		// nolint:ineffassign,staticcheck
		errs = append(errs, field.Invalid(fieldPath, val, err.Error()))
		mu.Unlock()
	}
	wg.Done()
}

func (r *FlatTrack) validate() error {
	var allErrs field.ErrorList
	var wg sync.WaitGroup
	var mu sync.Mutex

	tests := []test{
		{
			value:     r.Spec.Image,
			fieldPath: field.NewPath("spec").Child("image"),
			fn: func() error {
				if _, err := crane.Digest(*r.Spec.Image); err != nil {
					return err
				}
				return nil
			},
		},
		{
			value:     r.Spec.Postgres.Image,
			fieldPath: field.NewPath("spec").Child("postgres").Child("image"),
			fn: func() error {
				if _, err := crane.Digest(*r.Spec.Postgres.Image); err != nil {
					return err
				}
				return nil
			},
		},
		{
			value:     r.Spec.BackupCronJob.ImageClientPostgres,
			fieldPath: field.NewPath("spec").Child("backupCronJob").Child("imageClientPostgres"),
			fn: func() error {
				if _, err := crane.Digest(*r.Spec.BackupCronJob.ImageClientPostgres); err != nil {
					return err
				}
				return nil
			},
		},
		{
			value:     r.Spec.BackupCronJob.ImageClientMinio,
			fieldPath: field.NewPath("spec").Child("backupCronJob").Child("imageClientMinio"),
			fn: func() error {
				if _, err := crane.Digest(*r.Spec.BackupCronJob.ImageClientMinio); err != nil {
					return err
				}
				return nil
			},
		},
		{
			value:     r.Spec.SchedulerCronJob.Image,
			fieldPath: field.NewPath("spec").Child("schedulerCronJob").Child("image"),
			fn: func() error {
				if _, err := crane.Digest(*r.Spec.SchedulerCronJob.Image); err != nil {
					return err
				}
				return nil
			},
		},
		{
			value:     r.Spec.Postgres.PersistentVolumeSize,
			fieldPath: field.NewPath("spec").Child("postgres").Child("persistentVolumeSize"),
			fn: func() error {
				if _, err := resource.ParseQuantity(*r.Spec.Postgres.PersistentVolumeSize); err != nil {
					return err
				}
				return nil
			},
		},
	}
	for _, t := range tests {
		t := t
		go func(t test) {
			r.validateField(&wg, &mu, allErrs, t.value, t.fieldPath, t.fn)
		}(t)
	}
	wg.Wait()
	if len(allErrs) == 0 {
		return nil
	}
	return apierrors.NewInvalid(
		GroupVersion.WithKind("FlatTrack").GroupKind(),
		r.Name, allErrs)
}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *FlatTrack) Default() {
	flattracklog.Info("default", "name", r.Name)
}

//+kubebuilder:webhook:path=/validate-instances-flattrack-io-v1alpha1-flattrack,mutating=false,failurePolicy=fail,sideEffects=None,groups=instances.flattrack.io,resources=flattracks,verbs=create;update,versions=v1alpha1,name=vflattrack.kb.io,admissionReviewVersions=v1

var _ webhook.Validator = &FlatTrack{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *FlatTrack) ValidateCreate() (admission.Warnings, error) {
	flattracklog.Info("validate create", "name", r.Name)
	if err := r.validate(); err != nil {
		return nil, err
	}
	return nil, nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *FlatTrack) ValidateUpdate(old runtime.Object) (admission.Warnings, error) {
	flattracklog.Info("validate update", "name", r.Name)
	if err := r.validate(); err != nil {
		return nil, err
	}
	return nil, nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *FlatTrack) ValidateDelete() (admission.Warnings, error) {
	flattracklog.Info("validate delete", "name", r.Name)

	return nil, nil
}
